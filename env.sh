#!/bin/bash
export NCBI_GEO_DATADIR='/Users/guo/Desktop/GEO_data/GSE133486/iWAT_nuclei'  #GEO_data_dir
export PATH=$PATH:$NCBI_GEO_DATADIR

export KOE_PCRDIR='/Users/guo/Desktop/GEO_data/GSE133486/ko_efficiency_experiment/20200616' #curation_pcr
export PATH=$PATH:$KOE_PCRDIR